import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';

import htmlToImage from 'html-to-image';
import * as jsPDF from 'jspdf';
import { File, IWriteOptions } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public folder: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    public loadingCtrl: LoadingController,
    private file: File,
    private fileOpener: FileOpener
  ) { }

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id');
  }
  //html to pdf code start 

  loading: any;
  async presentLoading(msg) {
    const loading = await this.loadingCtrl.create({
      message: msg
    }).then(a => {
      a.present().then(() => {
        if (!this.loading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });;
  }
  async dismiss() {
    this.loading = false;
    return await this.loadingCtrl.dismiss().then(() => console.log('dismissed'));
  }
  downloadPdf() {
    this.presentLoading('Creating PDF file...');
    const node = document.getElementById("convert-html");
    var vm = this;

    htmlToImage.toPng(node)
      .then(function (dataUrl) {
        var img = new Image();
        img.src = dataUrl;
        var doc = new jsPDF('p', 'px', 'letter');
        //Add image Url to PDF

        var width = doc.internal.pageSize.getWidth();
        var height = doc.internal.pageSize.getHeight();
        console.log(width, 'X', height);

        doc.addImage(dataUrl, 'PNG', 0, 0, width, height);

        let pdfOutput = doc.output();
        // using ArrayBuffer will allow you to put image inside PDF
        let buffer = new ArrayBuffer(pdfOutput.length);
        let array = new Uint8Array(buffer);
        for (var i = 0; i < pdfOutput.length; i++) {
          array[i] = pdfOutput.charCodeAt(i);
        }

        //This is where the PDF file will stored , you can change it as you like
        // for more information please visit https://ionicframework.com/docs/native/file/
        const directory = vm.file.dataDirectory;
        const fileName = "invoice.pdf";
        let options: IWriteOptions = { replace: true };

        vm.file.checkFile(directory, fileName).then((success) => {
          //Writing File to Device
          vm.file.writeFile(directory, fileName, buffer, options)
            .then((success) => {
              vm.dismiss();
              console.log("File created Succesfully" + JSON.stringify(success));
              vm.fileOpener.open(vm.file.dataDirectory + fileName, 'application/pdf')
                .then(() => console.log('File is opened'))
                .catch(e => console.log('Error opening file', e));
            })
            .catch((error) => {
              vm.dismiss();
              console.log("Cannot Create File " + JSON.stringify(error));
            });
        })
          .catch((error) => {
            //Writing File to Device
            vm.file.writeFile(directory, fileName, buffer)
              .then((success) => {
                vm.dismiss();
                console.log("File created Succesfully" + JSON.stringify(success));
                vm.fileOpener.open(vm.file.dataDirectory + fileName, 'application/pdf')
                  .then(() => console.log('File is opened'))
                  .catch(e => console.log('Error opening file', e));
              })
              .catch((error) => {
                vm.dismiss();
                console.log("Cannot Create File " + JSON.stringify(error));
              });
          });
      })
      .catch(function (error) {
        console.error('oops, something went wrong!', error);
      });
  }
}
